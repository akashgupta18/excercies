function keys(Obj){
    let key =[];
    if(Obj === undefined){
        return {};
    }
    for(let i in Obj){
        key.push(i);
    }
    return key;
}

module.exports = keys;