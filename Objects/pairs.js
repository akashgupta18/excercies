function pairs(obj){
    let result =[];
    if(obj === undefined){
        return {};
    }
    for(let i in obj){
        result.push([i, obj[i]]);
    }
    return result;

}

module.exports = pairs;