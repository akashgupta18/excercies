function mapObject(Object, cb){
    let result = {};
    for(let i in Object){
        result[i] = cb(Object[i]);
    }
    return result;
}

module.exports = mapObject;