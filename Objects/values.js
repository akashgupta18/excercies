function values(Obj){
    let value =[];
    if(Obj === undefined){
        return {};
    }
    for(let i in Obj){
        value.push(Obj[i]);
    }
    return value;
}

module.exports = values;