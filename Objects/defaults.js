function defaults(obj, defaultProps){
    let props = defaultProps;
    for(let i in props){
        if(obj[i] === undefined){
            obj[i] = props[i];
        }
    }
    return obj;
}

module.exports = defaults;