const keys = require('./keys.js');
const values = require('./values.js');
const mapObject = require('./mapObject.js');
const pairs = require('./pairs.js');
const invert = require('./invert.js');
const defaults = require('./defaults.js');



const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

// Test keys()

console.log(keys(testObject));

// Test values()

console.log(values(testObject));

// Test mapObject()
let add = (x) => x+5;

console.log(mapObject(testObject, add));

// Test pairs()

console.log(pairs(testObject));

// Test pairs()

console.log(invert(testObject));

// Test defaults()

console.log(defaults(testObject, {'name':'Tony stark', 'character': 'Batman'}));