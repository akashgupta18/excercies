let result = [];

function flatten(elements) {

    for(let i = 0; i<elements.length; i++){
         if(Array.isArray(elements[i]) !== true){
             result.push(elements[i]);
         }
         if(Array.isArray(elements[i])){
             flatten(elements[i]);
         }
    }
    return result;
}

module.exports = flatten;