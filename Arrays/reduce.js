function reduce(elements, cb, startingValue){
    let itr = 0;
    if(startingValue === undefined){
        startingValue = elements[0];
        itr = 1;
    }
    for(let i = itr; i < elements.length; i++){
        startingValue = cb(startingValue,elements[i],i,elements);
    }
    return startingValue;
}


module.exports = reduce;