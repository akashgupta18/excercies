const each = require("./each.js");
const map = require("./map.js");
const find = require("./find.js");
const filter = require("./filter.js");
const reduce = require("./reduce.js");
const flatten = require("./flatten.js");


// forEach() testing

const items = [1, 2, 3, 4, 5, 5]; 

each(items, function(num){
    console.log('Square of ' + num + ' is ' + num*num);
});

// map() testing
let square = map(items, function(item){
    return item*item; 
});
console.log(square);

let dict = [
    {price:"100", product: "Pen"},
    {price:"300", product: "Box"},
    {price:"400", product: "Book"}
];

let price = map(dict, function(item){
    return item.price;
});
console.log(price);


// find() testing

function isEven(num){
    return num%2 === 0;
}

function grater_than_2(num){
    return num > 2;
}

console.log(find(items,isEven));
console.log(find(items,grater_than_2));


// filter() testing

function isEven(num){
    return num%2 === 0;
}

console.log(filter(items, isEven));

let emp = [
    {name:'Erik', salary: 50000},
    {name:'Bob', salary: 15000},
    {name:'Tony', salary: 10000},
    {name:'Akash', salary: 9000},
    {name:'Om', salary: 25000}        
];

const salary = filter(emp, (num) => num.salary >10000);
console.log(salary);


// reduce() testing

function add(acc,cv){
    return acc+cv;
}
console.log(reduce(items,add,5));

// flatten() testing
const nestedArray = [1, [2], [[3]], [[[4]]]]; 

console.log(flatten(nestedArray));