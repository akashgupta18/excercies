function each(arr, cb){
    if(arr.length !== 0 && typeof cb === 'function'){
        for(let i = 0; i < arr.length; i++){
            cb(arr[i] , i, arr);
        }
    }
    else {
        return [];
    }
}

module.exports = each;