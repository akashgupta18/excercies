function cacheFunction(cb) {
    let cache = {};
    return function(arg){
        if(cache[arg]){
            console.log("cache has that property");
            return cache[arg];
        }else {
            console.log('Added into cache');
            let result = cb(arg);
            return cache[arg] = result;
        }
    }
}

module.exports = cacheFunction;