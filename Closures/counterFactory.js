function counterFactory(){
    let initialValue = 0;
    let obj = {};
    obj.increment = function(){
        initialValue++;
        return initialValue;
    }
    obj.decrement = function(){
        initialValue--;
        return initialValue;
    }
    return obj;
}




module.exports = counterFactory;