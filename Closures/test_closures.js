const counterFactory = require('./counterFactory.js');
const limitFunctionCallCount = require('./limitFunctionCallCount.js');
const cacheFunction = require('./cacheFunction.js');


// counterFactory() Testing

var counter = counterFactory();

console.log(counter.increment());
console.log(counter.increment());
console.log(counter.increment());
console.log(counter.increment());
console.log(counter.decrement());

// limitFunctionCallCount() Testing

function sayHello(){
    console.log("hello");
}
let result = limitFunctionCallCount(sayHello,5);
result();
result();
result();
result();
result();
result();
result();

// cache() Testing
function cb(arg){
    return arg*5;
}
var test = cacheFunction(cb);

console.log(test(11));
console.log(test(5));
console.log(test(11));
console.log(test(5));
