function limitFunctionCallCount(cb, n){
    let count = 0;
    function functionCount(){
        if(count < n){
            count++;
            return cb();
        }
    }
    return functionCount;
}

module.exports = limitFunctionCallCount;